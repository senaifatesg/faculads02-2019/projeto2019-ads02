
package br.controle;

import br.conexao.ClienteTCP;
import java.io.IOException;
import cadastrodeveiculos.interfaces.TransformaDados;
import javax.swing.JOptionPane;

public class ControleContato {
    
    private static ControleContato objCtrl;
    private ClienteTCP comunicacao = null;
    
    public ControleContato() throws IOException{
        comunicacao = new ClienteTCP("10.144.31.195",7777);
    }
    
    public void incluirDadosPersistencia(Object objeto, int operacao)throws Exception{
        try {
            
            
            String msg = objeto.getClass().getSimpleName()+"#"+operacao+"#";
            TransformaDados dado = (TransformaDados)objeto;
            msg += dado.desmaterializar();
            JOptionPane.showMessageDialog(null, msg);
            comunicacao.enviarMensagem(msg);
            String msgRecebido = comunicacao.receberMensagem();
            System.out.println(msgRecebido);
        } catch (Exception erro) {
            throw erro;
        }     
    }
    public static ControleContato getInstance() throws IOException{
        if(objCtrl == null){
            objCtrl = new ControleContato();
        }
        return objCtrl;
    }
}
