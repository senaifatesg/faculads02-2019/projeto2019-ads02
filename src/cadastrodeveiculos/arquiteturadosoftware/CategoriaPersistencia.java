/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodeveiculos.arquiteturadosoftware;

import cadastrodeveiculos.interfaces.CrudCategoria;
import cadastrodeveiculos.classe.Categoria;
import java.io.*;
import java.util.ArrayList;
/**
 *
 * @author Fernando
 */
public class CategoriaPersistencia implements CrudCategoria{
    
    private String CategoriaCadastro=null;
    
    public CategoriaPersistencia(String CategoriaCadastro){
        this.CategoriaCadastro = CategoriaCadastro;
    }

    @Override
    public void incluir(Categoria objeto) throws Exception {
        try{
         //cria o arquivo
         FileWriter fw = new FileWriter(CategoriaCadastro,true);
         //Criar o buffer do arquivo
         BufferedWriter bw =new BufferedWriter(fw);
         //Escreve no arquivo
         bw.write(objeto.desmaterializar()+"\n");
         //fecha o arquivo
         bw.close();		
      }catch(Exception erro){
         throw erro;
      }
    }

    
    
    @Override
    public ArrayList<Categoria> listagem() throws Exception {
        try{
         ArrayList<Categoria> vetorDeContatos = new ArrayList<>();
         FileReader fr = new FileReader(CategoriaCadastro);
         BufferedReader br  = new BufferedReader(fr);
         String linha = "";
         while((linha=br.readLine())!=null){
                Categoria objCategoria = new Categoria();
                objCategoria.materializar(linha);
                vetorDeContatos.add(objCategoria);
            }
         br.close();
         return vetorDeContatos;
      }catch(Exception erro){
         throw erro;
      }
    }

    @Override
    public void excluir(String categoria) throws Exception {

        try{
         ArrayList<Categoria> vetorDeContatos = listagem();
         //cria o arquivo
         FileWriter fw = new FileWriter(CategoriaCadastro);
         //Criar o buffer do arquivo
         BufferedWriter bw =new BufferedWriter(fw);
         
         for(int pos = 0; pos < vetorDeContatos.size(); pos++){
            Categoria aux = vetorDeContatos.get(pos);
            if((aux.getCategoria().equals(categoria))){
               bw.write(aux.desmaterializar()+"\n");
            }
        }
            bw.close();
        }catch(Exception erro){
            throw erro;
        }
    }  
    
}
