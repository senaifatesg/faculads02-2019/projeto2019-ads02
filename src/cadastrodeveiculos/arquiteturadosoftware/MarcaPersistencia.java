/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodeveiculos.arquiteturadosoftware;

import cadastrodeveiculos.classe.Marca;
import cadastrodeveiculos.interfaces.CRUD;
import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author Fernando
 */
public class MarcaPersistencia implements CRUD {
    private String marcaCadastro=null;
    
    public MarcaPersistencia(String marcaCadastro){
        this.marcaCadastro = marcaCadastro;
    }
    
    public void incluir(Marca objeto) throws Exception{
        try{
         //cria o arquivo
         FileWriter fw = new FileWriter(marcaCadastro,true);
         //Criar o buffer do arquivo
         BufferedWriter bw =new BufferedWriter(fw);
         //Escreve no arquivo
         bw.write(objeto.desmaterializar()+"\n");
         //fecha o arquivo
         bw.close();		
      }catch(Exception erro){
         throw erro;
      }

    }

    @Override
    public ArrayList<Marca> listagem() throws Exception {
        try{
         ArrayList<Marca> vetorDeContatos = new ArrayList<>();
         FileReader fr = new FileReader(marcaCadastro);
         BufferedReader br  = new BufferedReader(fr);
         String linha = "";
         while((linha=br.readLine())!=null){
                Marca objMarca = new Marca();
                objMarca.materializar(linha);
                vetorDeContatos.add(objMarca);
            }
         br.close();
         return vetorDeContatos;
      }catch(Exception erro){
         throw erro;
      }
    }

    
    @Override
    public void excluir(String marca) throws Exception {
        try{
         ArrayList<Marca> vetorDeContatos = listagem();
         //cria o arquivo
         FileWriter fw = new FileWriter(marcaCadastro);
         //Criar o buffer do arquivo
         BufferedWriter bw =new BufferedWriter(fw);
         
         for(int pos = 0; pos < vetorDeContatos.size(); pos++){
            Marca aux = vetorDeContatos.get(pos);
            if((aux.getMarca().equals(marca))){
               bw.write(aux.desmaterializar()+"\n");
            }
        }
            bw.close();
        }catch(Exception erro){
            throw erro;
        }
    }  

}
