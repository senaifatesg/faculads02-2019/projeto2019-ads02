/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodeveiculos.arquiteturadosoftware;

import cadastrodeveiculos.classe.Modelo;
import cadastrodeveiculos.interfaces.CrudModelo;
import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author Fernando
 */


public class ModeloPersistencia implements CrudModelo{
    private String ModeloCadastro = null;
    
public ModeloPersistencia(String ModeloCadastro){
        this.ModeloCadastro = ModeloCadastro;
    }    
    
 public void incluir(Modelo objeto) throws Exception{
        try{
         //cria o arquivo
         FileWriter fw = new FileWriter(ModeloCadastro,true);
         //Criar o buffer do arquivo
         BufferedWriter bw =new BufferedWriter(fw);
         //Escreve no arquivo
         bw.write(objeto.desmaterializar()+"\n");
         //fecha o arquivo
         bw.close();		
      }catch(Exception erro){
         throw erro;
      }
    }

    @Override
    public ArrayList<Modelo> listagem() throws Exception {
        try{
         ArrayList<Modelo> vetorDeContatos = new ArrayList<>();
         FileReader fr = new FileReader(ModeloCadastro);
         BufferedReader br  = new BufferedReader(fr);
         String linha = "";
         while((linha=br.readLine())!=null){
                Modelo objModelo = new Modelo();
                objModelo.materializar(linha);
                vetorDeContatos.add(objModelo);
            }
         br.close();
         return vetorDeContatos;
      }catch(Exception erro){
         throw erro;
      }
    }

    @Override
    public void excluir(String modelo) throws Exception {
        try{
         ArrayList<Modelo> vetorDeContatos = listagem();
         //cria o arquivo
         FileWriter fw = new FileWriter(ModeloCadastro);
         //Criar o buffer do arquivo
         BufferedWriter bw =new BufferedWriter(fw);
         
         for(int pos = 0; pos < vetorDeContatos.size(); pos++){
            Modelo aux = vetorDeContatos.get(pos);
            if((aux.getModelo().equals(modelo))){
               bw.write(aux.desmaterializar()+"\n");
            }
        }
            bw.close();
        }catch(Exception erro){
            throw erro;
        }       
    }
}