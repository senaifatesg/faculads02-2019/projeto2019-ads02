/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodeveiculos.arquiteturadosoftware;

import cadastrodeveiculos.arquiteturadosoftware.PersistenciaPessoaFisica;
import cadastrodeveiculos.classe.PessoaJuridica;
import cadastrodeveiculos.interfaces.CRUDJuridica;
import java.io.*;
import java.util.ArrayList;

    public class PersistenciaJuridica implements CRUDJuridica {
     
    private String nomeDoArquivoNoDisco=null;
    
    public PersistenciaJuridica(String nomeDoArquivoNoDisco){
        this.nomeDoArquivoNoDisco = nomeDoArquivoNoDisco;
    }

    public PersistenciaJuridica(PersistenciaPessoaFisica fisica) {}
    
    
  public void incluir(PessoaJuridica objeto) throws Exception {
        try{
         //cria o arquivo
         FileWriter fw = new FileWriter(nomeDoArquivoNoDisco,true);
            //Escreve no arquivo
            try ( //Criar o buffer do arquivo
                    BufferedWriter bw = new BufferedWriter(fw)) {
                //Escreve no arquivo
                bw.write(objeto.desmaterializar()+"\n");
                //fecha o arquivo
            }
      }catch(Exception erro){
         throw erro;
      }

    }

    @Override
    public ArrayList<PessoaJuridica> listagem() throws Exception {
        try{
         ArrayList<PessoaJuridica> vetorDeJuridica = new ArrayList<>();
         FileReader fr = new FileReader(nomeDoArquivoNoDisco);
         BufferedReader br  = new BufferedReader(fr);
         String linha = "";
         while((linha=br.readLine())!=null){
                PessoaJuridica objpessaoJuridica = new PessoaJuridica();
                objpessaoJuridica.materializar(linha);
                vetorDeJuridica.add(objpessaoJuridica);
            }
         br.close();
         return vetorDeJuridica;
      }catch(Exception erro){
         throw erro;
      }
    }


    
    @Override
    public void excluir(String CNPJ) throws Exception {
        try{
         ArrayList<PessoaJuridica> vetorPessoaJuridica = listagem();
         //cria o arquivo
         FileWriter fw = new FileWriter(nomeDoArquivoNoDisco);
         //Criar o buffer do arquivo
         BufferedWriter bw =new BufferedWriter(fw);
         for(int pos = 0; pos < vetorPessoaJuridica.size(); pos++){
            PessoaJuridica aux = vetorPessoaJuridica.get(pos);
            if(!(aux.getCNPJ().equals(CNPJ))){
               bw.write(aux.desmaterializar()+"\n");
         }
         }
         bw.close();
      }catch(Exception erro){
         throw erro;
      }
    }

}
