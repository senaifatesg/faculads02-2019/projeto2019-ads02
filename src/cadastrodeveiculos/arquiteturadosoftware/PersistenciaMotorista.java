/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodeveiculos.arquiteturadosoftware;

import cadastrodeveiculos.arquiteturadosoftware.PersistenciaPessoaFisica;
import cadastrodeveiculos.classe.PessoaMotorista;
import cadastrodeveiculos.interfaces.CRUDMotorista;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class PersistenciaMotorista implements CRUDMotorista{
      private String nomeDoArquivoNoDisco=null;
    
    public PersistenciaMotorista(String nomeDoArquivoNoDisco){
        this.nomeDoArquivoNoDisco = nomeDoArquivoNoDisco;
    }

    public PersistenciaMotorista(PersistenciaPessoaFisica fisica) {}
    
    
  public void incluir(PessoaMotorista objeto) throws Exception {
        try{
         //cria o arquivo
         FileWriter fw = new FileWriter(nomeDoArquivoNoDisco,true);
            //Escreve no arquivo
            try ( //Criar o buffer do arquivo
                    BufferedWriter bw = new BufferedWriter(fw)) {
                //Escreve no arquivo
                bw.write(objeto.desmaterializar()+"\n");
                //fecha o arquivo
            }
      }catch(Exception erro){
         throw erro;
      }

    }

    public ArrayList<PessoaMotorista> listagem() throws Exception {
        try{
         ArrayList<PessoaMotorista> vetorDeMotorista = new ArrayList<>();
         FileReader fr = new FileReader(nomeDoArquivoNoDisco);
         BufferedReader br  = new BufferedReader(fr);
         String linha = "";
         while((linha=br.readLine())!=null){
                PessoaMotorista objpessaoMotorista = new PessoaMotorista();
                objpessaoMotorista.materializar(linha);
                vetorDeMotorista.add(objpessaoMotorista);
            }
         br.close();
         return vetorDeMotorista;
      }catch(Exception erro){
         throw erro;
      }
    }


    
    public void excluir(String CNH) throws Exception {
        try{
         ArrayList<PessoaMotorista> vetorPessoaMotorista = listagem();
         //cria o arquivo
         FileWriter fw = new FileWriter(nomeDoArquivoNoDisco);
         //Criar o buffer do arquivo
         BufferedWriter bw =new BufferedWriter(fw);
         for(int pos = 0; pos < vetorPessoaMotorista.size(); pos++){
            PessoaMotorista aux = vetorPessoaMotorista.get(pos);
            if(!(aux.getnRegistro().equals(CNH))){
               bw.write(aux.desmaterializar()+"\n");
         }
         }
         bw.close();
      }catch(Exception erro){
         throw erro;
      }
    }
}
