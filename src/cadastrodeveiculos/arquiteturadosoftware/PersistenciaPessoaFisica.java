/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cadastrodeveiculos.arquiteturadosoftware;
import cadastrodeveiculos.classe.PessoaFisica;
import cadastrodeveiculos.interfaces.CRUDFisica;
import java.io.*;
import java.util.ArrayList;
/**
 *
 * @author eugeniojulio
 */
public class PersistenciaPessoaFisica implements CRUDFisica {

    private String nomeDoArquivoNoDisco=null;
    
    public PersistenciaPessoaFisica(String nomeDoArquivoNoDisco){
        this.nomeDoArquivoNoDisco = nomeDoArquivoNoDisco;
    }

    
    @Override
    public void incluir(PessoaFisica objeto) throws Exception{
        try{
         //cria o arquivo
         FileWriter fw = new FileWriter(nomeDoArquivoNoDisco,true);
            //Escreve no arquivo
            try ( //Criar o buffer do arquivo
                    BufferedWriter bw = new BufferedWriter(fw)) {
                //Escreve no arquivo
                bw.write(objeto.desmaterializar()+"\n");
                //fecha o arquivo
            }
      }catch(Exception erro){
         throw erro;
      }

    }

    @Override
    public ArrayList<PessoaFisica> listagem() throws Exception {
        try{
         ArrayList<PessoaFisica> vetorDeContatos = new ArrayList<>();
         FileReader fr = new FileReader(nomeDoArquivoNoDisco);
         BufferedReader br  = new BufferedReader(fr);
         String linha = "";
         while((linha=br.readLine())!=null){
                PessoaFisica objpessaofisica = new PessoaFisica();
                objpessaofisica.materializar(linha);
                vetorDeContatos.add(objpessaofisica);
            }
         br.close();
         return vetorDeContatos;
      }catch(Exception erro){
         throw erro;
      }
    }


    
    @Override
    public void excluir(String nome) throws Exception {
        try{
         ArrayList<PessoaFisica> vetorPessoaFisica = listagem();
         //cria o arquivo
         FileWriter fw = new FileWriter(nomeDoArquivoNoDisco);
         //Criar o buffer do arquivo
         BufferedWriter bw =new BufferedWriter(fw);
         for(int pos = 0; pos < vetorPessoaFisica.size(); pos++){
            PessoaFisica aux = vetorPessoaFisica.get(pos);
            if(!(aux.getNome().equals(nome))){
               bw.write(aux.desmaterializar()+"\n");
         }
         }
         bw.close();
      }catch(Exception erro){
         throw erro;
      }
    }

    }


    

