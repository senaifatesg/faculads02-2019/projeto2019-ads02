/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodeveiculos.arquiteturadosoftware;

import cadastrodeveiculos.classe.Veiculos;
import cadastrodeveiculos.interfaces.CrudVeiculo;
import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author Fernando
 */
public class VeiculoPersistencia implements CrudVeiculo{
        private String VeiculoCadastro = null;
    
public VeiculoPersistencia(String VeiculoCadastro){
        this.VeiculoCadastro = VeiculoCadastro;
    }    
    
@Override
    public void incluir(Veiculos objeto) throws Exception {
       try{
         //cria o arquivo
         FileWriter fw = new FileWriter(VeiculoCadastro,true);
         //Criar o buffer do arquivo
         BufferedWriter bw =new BufferedWriter(fw);
         //Escreve no arquivo
         bw.write(objeto.desmaterializar()+"\n");
         //fecha o arquivo
         bw.close();		
      }catch(Exception erro){
         throw erro;
      }
    }


    @Override
    public ArrayList<Veiculos> listagem() throws Exception {
        
         try{
         ArrayList<Veiculos> vetorDeContatos = new ArrayList<>();
         FileReader fr = new FileReader(VeiculoCadastro);
         BufferedReader br  = new BufferedReader(fr);
         String linha = "";
         while((linha=br.readLine())!=null){
                Veiculos objVeiculo = new Veiculos();
                objVeiculo.materializar(linha);
                vetorDeContatos.add(objVeiculo);
            }
         br.close();
         return vetorDeContatos;
      }catch(Exception erro){
         throw erro;
      }
     
    }
    
    @Override
    public void excluir(String modelo) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    

 
}
