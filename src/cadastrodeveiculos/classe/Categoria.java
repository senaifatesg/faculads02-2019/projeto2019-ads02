/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodeveiculos.classe;

import cadastrodeveiculos.interfaces.TransformaDados;

/**
 *
 * @author Fernando
 */
public class Categoria implements TransformaDados {
    
    //Atributos
    
     private String categoria = "";
     private String codigo;
     
     public Categoria (){}
     public Categoria (String codigo, String categoria){
         this.categoria = categoria;
         this.codigo = codigo;
     }
     
    
     //Metodos

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getCodigo(){
        return codigo;
    }  
    public void setCodigo(String codigo){
        this.codigo = codigo;
    }    
    
    // Override
    
    @Override
    public String desmaterializar(){
        
        String saida = categoria + ";";
        saida += codigo;
        
        return saida;
    }

    public void materializar (String str) throws Exception{
        
        String vetorString [] = str.split(";");
        if(vetorString.length != 2) throw new Exception ("Faltam dados para serem preenchidos");
        
        categoria = vetorString[0];
        codigo = vetorString[1];
          
    } 
}
