/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodeveiculos.classe;

import cadastrodeveiculos.interfaces.TransformaDados;
/**
 *
 * @author Fernando
 */
public class Marca implements TransformaDados {
    
    //Atributos
    
     private String marca = "";
     private String codigo;
     
     public Marca(){}
     public Marca (String marca, String codigo){
         this.marca = marca;
         this.codigo = codigo;
     }
     
    
     //Metodos

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getCodigo(){
        return codigo;
    }  
    public void setCodigo(String codigo){
        this.codigo = codigo;
    }    
    
    // Override
    
    @Override
    public String desmaterializar(){
        
        String saida = marca + ";";
        saida += codigo;
        
        return saida;
    }

    @Override
    public void materializar (String str) throws Exception{
        
        String vetorString [] = str.split(";");
        if(vetorString.length != 2) throw new Exception ("Faltam dados para serem preenchidos");
        
        marca = vetorString[0];
        codigo = vetorString[1];
      //  ano = vetorString[2];     
    } 
}
