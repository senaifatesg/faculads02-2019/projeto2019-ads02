/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodeveiculos.classe;

import cadastrodeveiculos.interfaces.TransformaDados;

/**
 *
 * @author Fernando
 */
public class Modelo implements TransformaDados{
    
    
    // Atributos
    
    private String codigoM;
    private String modelo = "";
    private int ano = 0;
    
    //Metodos
    
    public Modelo(){}
    public Modelo (String codigoM, String modelo, int ano){
        this.codigoM = codigoM;
        this.modelo = modelo;
        this.ano = ano;
    }
    
    //Get's & Set's
    public String getCodigoM(){
        return codigoM;
    }
    public void setCodigoM (String codigoM){
        this.codigoM = codigoM;
    }
    
    public String getModelo(){
       return modelo;
    }
    public void setModelo (String modelo){
        this.modelo = modelo;
    }
    
    public int getAno(){
        return ano;
    }
    public void setAno(int ano){
        this.ano = ano;
    }
    
    //Override
    
    @Override
    public String desmaterializar (){
        
        String saida = codigoM + ";";
        saida += modelo + ";";
        saida += ano;
        
        return saida;
    }
    
    @Override
    public void materializar (String str) throws Exception {
        String vetorString [] = str.split(";");
        if(vetorString.length != 3) throw new Exception ("Erro ao Salvar. Comunique o Criador do projeto");
        
        codigoM = vetorString[0];
        modelo = vetorString[1];
        ano = Integer.parseInt(vetorString[2]);     
    } 
}

   
