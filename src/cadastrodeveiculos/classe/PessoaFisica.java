
package cadastrodeveiculos.classe;
import cadastrodeveiculos.interfaces.TransformaDados;

public class PessoaFisica implements TransformaDados{
    private String id;
    private String CPF = "";
    private String nome = "";
    private String datanacimento = "";
    private String email = "";
    private String CEP = "";
    private String endereco = "";
    private String bairro = "";
    private String cidade = "";
    private String estado = "";
    private String celular = "";
    
    public PessoaFisica(String nome,String CPF,String datanacimento,String UF,String CEP,String cidade,String bairro,String estado ,String endereco,String celular, String email, String id) {
        this.id=id;
        this.CPF = CPF;
        this.nome = nome;
        this.datanacimento = datanacimento;
        this.email = email;
        this.CEP = CEP;
        this.endereco = endereco;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
        this.celular = celular;

    }

    public PessoaFisica() {}
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * @return the CPF
     */
    public String getCPF() {
        return CPF;
    }

    /**
     * @param CPF the CPF to set
     */
    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the dataNasc
     */
    public String getDatanacimento() {
        return datanacimento;
    }

    /**
     * @param datanacimento the dataNasc to set
     */
    public void setdatanacimento(String datanacimento) {
        this.datanacimento = datanacimento;
    }

    /**
     * @return the Email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param Email the Email to set
     */
    public void setEmail(String Email) {
        this.email = Email;
    }

    /**
     * @return the CEP
     */
    public String getCEP() {
        return CEP;
    }

    /**
     * @param CEP the CEP to set
     */
    public void setCEP(String CEP) {
        this.CEP = CEP;
    }

    /**
     * @return the endereco
     */
    public String getEndereco() {
        return endereco;
    }

    /**
     * @param endereco the endereco to set
     */
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    /**
     * @return the bairro
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * @param bairro the bairro to set
     */
    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    /**
     * @return the cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * @param cidade the cidade to set
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the celular
     */
    public String getCelular() {
        return celular;
    }

    /**
     * @param celular the celular to set
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }
@Override
     public String desmaterializar() {
        String saida = id + ";";
         saida += nome  + ";";
        saida += CEP + ";";
        saida += CPF + ";";
        saida += bairro + ";";
        saida += celular + ";";
        saida += datanacimento + ";";
        saida += email + ";";
        saida += endereco + ";";
        saida += estado + ";";
        saida += cidade;
        return saida;
    }
@Override
     public void materializar(String str) throws Exception{
        String vetorString[] = str.split(";");
        if(vetorString.length != 11) throw new Exception("Faltam dados na String");
        id = vetorString[0];
        CEP =vetorString[1];
        CPF =vetorString[2];
        bairro =vetorString[3];
        celular =vetorString[4];
        datanacimento =vetorString[5];
        email =vetorString[6];
        endereco = vetorString[7];
        estado = vetorString[8];
        nome = vetorString[9];
        cidade = vetorString [10];
    }
     
         public boolean verificaCamposNulos() {
        if (this.CPF.equals("   .   .   -  ") || this.CEP.equals("  .   -  ") || this.nome.equals("") || this.endereco.equals("") || this.datanacimento.equals("  /  /    ")
                || this.bairro.equals("") || this.email.equals("") || this.celular.equals("") || this.cidade == null || this.estado == null  ) {
            return true;
        } else {
            return false;
        }

    }
}


    

