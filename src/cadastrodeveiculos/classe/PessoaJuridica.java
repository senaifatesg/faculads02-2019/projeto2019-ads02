
package cadastrodeveiculos.classe;

import cadastrodeveiculos.interfaces.TransformaDados;

public class PessoaJuridica implements TransformaDados{

    
    private String CNPJ = "";
    private String razaoSocial = "";
    private String nomeFantasia = "";
    private String inscricaoEstadual = "";
    private String endereco = "";
    private String CEP = "";
    private String bairro = "";
    private String cidade = "";
    private String estado = "";
    private String telefone = "";

    public PessoaJuridica() {}
    public PessoaJuridica (String nomefantasia, String razaosocial, String CNPJ, String inscricaoestadual, String CEP, String endereco, String estado, String cidade, String bairro, String telefone) {

        this.CNPJ = CNPJ;
        this.razaoSocial = razaoSocial;
        this.nomeFantasia = nomeFantasia;
        this.inscricaoEstadual = inscricaoEstadual;
        this.CEP = CEP;
        this.endereco = endereco;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
        this.telefone = telefone;
   

    }

    
        

  

    /**
     * @return the CNPJ
     */
    public String getCNPJ() {
        return CNPJ;
    }

    /**
     * @param CNPJ the CNPJ to set
     */
    public void setCNPJ(String CNPJ) {
        this.CNPJ = CNPJ;
    }

    /**
     * @return the razaoSocial
     */
    public String getRazaoSocial() {
        return razaoSocial;
    }

    /**
     * @param razaoSocial the razaoSocial to set
     */
    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    /**
     * @return the nomeFantasia
     */
    public String getNomeFantasia() {
        return nomeFantasia;
    }

    /**
     * @param nomeFantasia the nomeFantasia to set
     */
    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    /**
     * @return the inscricaoEstadual
     */
    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    /**
     * @param inscricaoEstadual the inscricaoEstadual to set
     */
    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    /**
     * @return the endereco
     */
    public String getEndereco() {
        return endereco;
    }

    /**
     * @param endereco the endereco to set
     */
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    /**
     * @return the CEP
     */
    public String getCEP() {
        return CEP;
    }

    /**
     * @param CEP the CEP to set
     */
    public void setCEP(String CEP) {
        this.CEP = CEP;
    }

    /**
     * @return the bairro
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * @param bairro the bairro to set
     */
    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    /**
     * @return the cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * @param cidade the cidade to set
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the telefone
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * @param telefone the telefone to set
     */
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
 
     public String desmaterializar() {
        String saida = razaoSocial +";";
        saida += CEP +";";
        saida += CNPJ +";";
        saida += bairro+";";
        saida += cidade +";";
        saida += endereco + ";";
        saida += estado + ";";
        saida += nomeFantasia + ";";
        saida += inscricaoEstadual + ";";
        saida += telefone+";";
        
        return saida;
    }
@Override
     public void materializar(String str) throws Exception{
        String vetorString[] = str.split(";");
        if(vetorString.length != 10) throw new Exception("Preencha os campos corretamente");
        razaoSocial = vetorString[0];
        CEP = vetorString [1];
        CNPJ = vetorString [2];
        bairro = vetorString [3];
        cidade = vetorString [4];
        endereco = vetorString [5];
        estado = vetorString [6];
        inscricaoEstadual = vetorString [7];
        nomeFantasia = vetorString [8];
        telefone = vetorString [9];
    

    }
    public boolean verificaCamposNulos() {
        if (CNPJ.equals("  .   .   /    -  ") || CEP.equals("  .   -   ") || this.razaoSocial.equals("") || this.nomeFantasia.equals("")
                || this.inscricaoEstadual.equals("") || this.endereco.equals("") || this.bairro.equals("") || this.telefone.equals("(  )       -    ")
              ||  this.cidade == null || this.estado == null ) {
            return true;
        } else {
            return false;
        }
    }
}
