
package cadastrodeveiculos.classe;

import cadastrodeveiculos.interfaces.TransformaDados;


public class PessoaMotorista implements TransformaDados{
    
    private String nome = "";
    private String dataNascimento = "";
    private String CPF = "";
    private String telefone = "";
    private String nRegistro = "";
    private String validadeCNH = "";

     private String id;

    public PessoaMotorista() {}

  public boolean verificaCamposNulos() {return false;
}
    
    public PessoaMotorista(String nome, String dataNascimemento, String CPF, String telefone, String nRegistro, String validadeCNH){
        this.nome = nome;
        this.dataNascimento = dataNascimemento;
        this.CPF = CPF;
        this.telefone = telefone;
        this.nRegistro = nRegistro;
        this.validadeCNH = validadeCNH;

    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the dataNascimento
     */
    public String getDataNascimento() {
        return dataNascimento;
    }

    /**
     * @param dataNascimento the dataNascimento to set
     */
    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    /**
     * @return the CPF
     */
    public String getCPF() {
        return CPF;
    }

    /**
     * @param CPF the CPF to set
     */
    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    /**
     * @return the telefone
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * @param telefone the telefone to set
     */
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    /**
     * @return the nRegistro
     */
    public String getnRegistro() {
        return nRegistro;
    }

    /**
     * @param nRegistro the nRegistro to set
     */
    public void setnRegistro(String nRegistro) {
        this.nRegistro = nRegistro;
    }

    /**
     * @return the validadeCNH
     */
    public String getValidadeCNH() {
        return validadeCNH;
    }

    /**
     * @param validadeCNH the validadeCNH to set
     */
    public void setValidadeCNH(String validadeCNH) {
        this.validadeCNH = validadeCNH;
    }

       @Override
    public String desmaterializar() {
     String saida = nome +";";
        saida += dataNascimento +";";
        saida += CPF+";";
        saida +=  telefone+";";
        saida += nRegistro +";";
        saida += validadeCNH;
 
        
        return saida;
    }

       @Override
    public void materializar (String stringDados) throws Exception {
      String[] vetorString = stringDados.split(";");
        nome = vetorString[0];
        dataNascimento = vetorString [1];
        CPF = vetorString [2];
        telefone = vetorString [3];
        nRegistro = vetorString [4];
        validadeCNH = vetorString [5];

}

}
