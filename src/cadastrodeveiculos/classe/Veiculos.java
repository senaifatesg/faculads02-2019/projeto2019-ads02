/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodeveiculos.classe;

import cadastrodeveiculos.interfaces.TransformaDados;

/**
 *
 * @author Fernando
 */
public class Veiculos implements TransformaDados{
    
    //Atributos
    private String codigoV ;
    private String placa ;
    private String cidade ;
    private String uf;
    private String cor;
    private String chassi;
    private String renavam;
    private String combustivel;
    private String km;
    private String adicional;
    
    //Metodos
    
    public Veiculos(){}
    public Veiculos(String codigoV, String placa, String cidade, String uf, 
    String cor,String chassi,String renavam, String combustivel, String km, String adicional){
        this.codigoV = codigoV;
        this.placa = placa;
        this.cidade = cidade;
        this.uf = uf;
        this.cor = cor;
        this.chassi = chassi;
        this.renavam = renavam;
        this.combustivel = combustivel;
        this.km = km;
        this.adicional = adicional; 
    }

    //Get's e Set's
    public String getCodigoV() {
        return codigoV;
    }

    public void setCodigoV(String codigoV) {
        this.codigoV = codigoV;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getChassi() {
        return chassi;
    }

    public void setChassi(String chassi) {
        this.chassi = chassi;
    }

    public String getRenavam() {
        return renavam;
    }

    public void setRenavam(String renavam) {
        this.renavam = renavam;
    }

    public String getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getAdicional() {
        return adicional;
    }

    public void setAdicional(String adicional) {
        this.adicional = adicional;
    }
    
    //Override

    @Override
    public String desmaterializar() {
        
    
        String saida = codigoV + ";";
        saida += placa + ";";
        saida += cidade + ";";
        saida += uf + ";";
        saida += cor + ";";
        saida += chassi + ";";
        saida += renavam + ";";
        saida += combustivel + ";";
        saida += km + ";";
        saida += adicional;
        
        return saida;
    }
    @Override
    public void materializar(String str) throws Exception {
        String vetorString [] = str.split(";");
        if(vetorString.length != 10) throw new Exception ("Erro ao Listar. Comunique o Criador do projeto");
        
        codigoV = vetorString[0];
        placa = vetorString[1];
        cidade = vetorString[2];   
        uf = vetorString[3];
        cor = vetorString[4];
        chassi = vetorString[5];
        renavam = vetorString[6];
        combustivel = vetorString[7];
        km = vetorString[8];
        adicional = vetorString[9];
    }
       
}
