/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodeveiculos.interfaces;

import cadastrodeveiculos.classe.Marca;
import java.util.ArrayList;

/**
 *
 * @author Fernando
 */
public interface CRUD {
    
    //Marca
    void incluir(Marca objeto)throws Exception;
    ArrayList <Marca> listagem()throws Exception;
    void excluir(String marca) throws Exception;

}

