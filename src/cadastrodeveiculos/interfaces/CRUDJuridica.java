/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cadastrodeveiculos.interfaces;
import cadastrodeveiculos.classe.PessoaJuridica;
import java.util.ArrayList;
/**
 *
 * @author eugeniojulio
 */
public interface CRUDJuridica {
    void incluir(PessoaJuridica objeto)throws Exception;
    ArrayList<PessoaJuridica> listagem()throws Exception;
    void excluir(String nome) throws Exception;
}
