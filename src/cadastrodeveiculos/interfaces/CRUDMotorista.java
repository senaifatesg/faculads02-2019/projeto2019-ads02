
package cadastrodeveiculos.interfaces;

import cadastrodeveiculos.classe.PessoaMotorista;
import java.util.ArrayList;

public interface CRUDMotorista {
    void incluir(PessoaMotorista objeto)throws Exception;
    ArrayList<PessoaMotorista> listagem()throws Exception;
    void excluir(String CNH) throws Exception;
    
}
