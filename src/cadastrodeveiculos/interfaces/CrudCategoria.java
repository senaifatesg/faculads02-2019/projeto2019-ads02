/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodeveiculos.interfaces;

import cadastrodeveiculos.classe.Categoria;
import java.util.ArrayList;

/**
 *
 * @author Fernando
 */
public interface CrudCategoria {
    
        void incluir(Categoria objeto)throws Exception;
        ArrayList <Categoria> listagem()throws Exception;
        void excluir(String categoria) throws Exception;
    
}
