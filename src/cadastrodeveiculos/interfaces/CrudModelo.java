/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodeveiculos.interfaces;

import cadastrodeveiculos.classe.Modelo;
import java.util.ArrayList;

/**
 *
 * @author Fernando
 */
public interface CrudModelo {
    
    void incluir(Modelo objeto)throws Exception;
    ArrayList <Modelo> listagem()throws Exception;
    void excluir(String modelo) throws Exception;
    
}
