/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodeveiculos.interfaces;

import cadastrodeveiculos.classe.Veiculos;
import java.util.ArrayList;

/**
 *
 * @author Fernando
 */
public interface CrudVeiculo {
      void incluir(Veiculos objeto)throws Exception;
      ArrayList <Veiculos> listagem()throws Exception;
      void excluir(String modelo) throws Exception;
    
}
