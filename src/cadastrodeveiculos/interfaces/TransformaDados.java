/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodeveiculos.interfaces;

/**
 *
 * @author Fernando
 */
public interface TransformaDados {
    String desmaterializar();
    void materializar(String str)throws Exception;
}