package testes;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import cadastrodeveiculos.classe.Categoria;
import cadastrodeveiculos.classe.Modelo;

class CategoriaTest {

	@Test
	void testCategoria() {
		Categoria categ = new Categoria();
		categ.setCategoria("Luxo");
		String CategResult = categ.getCategoria();
		String valorEsperado = "Luxo";
		assertEquals(valorEsperado, CategResult);
		System.out.println("Obrigado Professora?");
	}

	@Test
	void testCodigo() {
		Categoria categ = new Categoria();
		categ.setCodigo("01");
		String CategResult = categ.getCodigo();
		String valorEsperado = "01";
		assertEquals(valorEsperado, CategResult);
		System.out.println("voc� sempre me ajudou");
	}

}
