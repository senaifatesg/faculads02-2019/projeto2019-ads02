package testes;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import cadastrodeveiculos.classe.Marca;

class MarcaTest {

	@Test
	void testMarca() {
		Marca m = new Marca();
		m.setMarca("Ford");
		String MarcaResult = m.getMarca();
		String Esperado = "Ford";
		assertEquals(Esperado, MarcaResult);
		System.out.println("Deu certo?");
	}
	

	@Test
	void testCodigo() {
		
		Marca m = new Marca();
		m.setCodigo("02");
		String codResult = m.getCodigo();
		String vEsperado = "02";
		assertEquals(vEsperado, codResult);	
		System.out.println("Funcionou agora!");
	}

}
