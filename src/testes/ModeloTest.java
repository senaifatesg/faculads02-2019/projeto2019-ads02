package testes;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import cadastrodeveiculos.classe.Modelo;

class ModeloTest {

	@Test
	void testModelo() {
		Modelo mod = new Modelo();
		mod.setModelo("Ka");
		String ModelResult = mod.getModelo();
		String vEsperado = "Ka";
		assertEquals(vEsperado, ModelResult);
		System.out.println("Ent�o, acho que falta informa��o?");
	}

	@Test
	void testCodigo() {
		Modelo mod = new Modelo();
		mod.setCodigoM("3");
		String ModelResult = mod.getCodigoM();
		String Esperado = "3";
		assertEquals(Esperado, ModelResult);
		System.out.println("voc� me ajuda por favor?");
	}
}
